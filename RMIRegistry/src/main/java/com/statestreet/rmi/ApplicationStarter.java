package com.statestreet.rmi;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

import com.statestreet.rmi.api.RmiUtils;

/**
 * @author artfable
 * @since 20/03/2022
 */
public class ApplicationStarter {


    public static void main(String[] args) throws RemoteException, InterruptedException {
        LocateRegistry.createRegistry(RmiUtils.RMI_REGISTRY_PORT);

        while (true) {
            Thread.sleep(100);
        }
    }
}
