plugins {
    `project-common`
    application
}

repositories {
    mavenLocal()
    mavenCentral()
}

application {
    mainClass.set("com.statestreet.rmi.ApplicationStarter")
}

dependencies {
    implementation(project(":rmi-api"))
}
