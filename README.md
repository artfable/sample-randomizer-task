# Overview

No additional libraries used for the application, however, Junit 5 is used for test.
Communication between services is done through RMI. 

Application is build and tested with java 17. As no syntax features after java 8 is used, should be possible to build and run with java 8.
However, `./buildSrc/src/main/kotlin/project-common.gradle.kts` should be modified in that case for right source/target compatibility.
Not tested with versions prior java 17.

## Run & Build

RMIRegistry should be started before Prime and Randomizer services.

For build:
```shell
./gradlew build
```

Each of 3 services will be build in `./<service>/build/distributions/<service>.(tar|zip)` and contain run shell and bat scripts in `/bin/`.

For running through docker:
For run:
```shell
docker-compose -f ./docker-compose.yml up -d --build
```

For checking Randomizer output:
```shell
docker logs statestreet_randomizer_1
```

For killing services:
```shell
docker-compose -f ./docker-compose.yml down
```