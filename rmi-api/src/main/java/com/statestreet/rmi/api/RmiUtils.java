package com.statestreet.rmi.api;

import java.rmi.RemoteException;
import java.rmi.registry.Registry;

/**
 * @author artfable
 * @since 20/03/2022
 */
public class RmiUtils {
    public static final int RMI_REGISTRY_PORT = 1099;

    public static void waitForRmiService(String name, Registry registry) throws RemoteException {
        while (true) {
            for (String registeredName: registry.list()) {
                if (name.equals(registeredName)) {
                    return;
                }
            }
        }
    }
}
