package com.statestreet.rmi.api.controller;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author artfable
 * @since 20/03/2022
 */
public interface RandomizerPrintController extends Remote {
    void print(int number, boolean prime) throws RemoteException;
}
