plugins {
    `project-common`
    application
}

repositories {
    mavenLocal()
    mavenCentral()
}

application {
    mainClass.set("com.statestreet.prime.ApplicationStarter")
}

dependencies {
    implementation(project(":rmi-api"))
}
