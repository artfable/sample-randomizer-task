package com.statestreet.prime.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author artfable
 * @since 20/03/2022
 */
class PrimeServiceTest {

    private PrimeService primeService;

    @BeforeEach
    void setUp() {
        primeService = new PrimeService();
    }

    @ParameterizedTest
    @CsvSource({"1,false", "2,true", "3,true", "17,true", "8,false", "15,false"})
    void isPrime(int number, boolean expectedPrime) {
        assertEquals(expectedPrime, primeService.isPrime(number));
    }

}