package com.statestreet.prime.service;

import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * Calculate if the number is prime. Keep cache with previously calculated number.
 * Alternatively, all prime numbers could be precalculated with Sieve of Eratosthenes algorithm.
 * <p>
 * Service expecting positive integers and doesn't provide any validation.
 *
 * @author artfable
 * @since 20/03/2022
 */
public class PrimeService {

    private final Set<Integer> primeCache = new ConcurrentSkipListSet<>();

    public PrimeService() {
        primeCache.add(2);
    }

    public boolean isPrime(int number) {
        if (primeCache.contains(number)) {
            return true;
        }

        if (number < 2 || number % 2 == 0) { // all even after 2 are not prime
            return false;
        }

        for (int i = 3; i < number / 2; i += 2) {
            if (number % i == 0) {
                return false;
            }
        }

        primeCache.add(number);
        return true;
    }
}
