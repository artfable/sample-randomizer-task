package com.statestreet.prime.service;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.statestreet.rmi.api.RmiUtils;
import com.statestreet.rmi.api.controller.RandomizerPrintController;

/**
 * Will keep infinite thread for calculation, allowing to schedule new checks through the queue.
 *
 * @author artfable
 * @since 20/03/2022
 */
public class PrimeCalculationTask implements Runnable {

    private final Registry registry;
    private final PrimeService primeService;

    private final Queue<Integer> queue = new ConcurrentLinkedQueue<>();

    public PrimeCalculationTask(Registry registry, PrimeService primeService) {
        this.registry = registry;
        this.primeService = primeService;
    }

    public void scheduleCheck(int number) {
        queue.add(number);
    }

    @Override
    public void run() {
        RandomizerPrintController randomizerPrint;
        String randomizerPrintName = "RandomizerPrint";

        try {
            RmiUtils.waitForRmiService(randomizerPrintName, registry);
            randomizerPrint = (RandomizerPrintController) registry.lookup(randomizerPrintName);
        } catch (RemoteException | NotBoundException e) {
            throw new RuntimeException(e);
        }

        while (true) {
            Integer number = queue.poll();
            if (number != null) {
                try {
                    randomizerPrint.print(number, primeService.isPrime(number));
                } catch (RemoteException e) {
                    e.printStackTrace(); // as can't use any logging lib, will just print to err
                }
            }
        }
    }
}
