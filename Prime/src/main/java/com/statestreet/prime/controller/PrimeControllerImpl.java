package com.statestreet.prime.controller;

import java.rmi.RemoteException;

import com.statestreet.prime.service.PrimeCalculationTask;
import com.statestreet.rmi.api.controller.PrimeController;

/**
 * @author artfable
 * @since 20/03/2022
 */
public class PrimeControllerImpl implements PrimeController {

    private final PrimeCalculationTask primeCalculationTask;

    public PrimeControllerImpl(PrimeCalculationTask primeCalculationTask) {
        this.primeCalculationTask = primeCalculationTask;
    }

    @Override
    public void sendInt(int number) throws RemoteException {
        primeCalculationTask.scheduleCheck(number);
    }
}
