package com.statestreet.prime;

import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.concurrent.Executors;

import com.statestreet.prime.controller.PrimeControllerImpl;
import com.statestreet.prime.service.PrimeCalculationTask;
import com.statestreet.prime.service.PrimeService;
import com.statestreet.rmi.api.RmiUtils;
import com.statestreet.rmi.api.controller.PrimeController;

/**
 * @author artfable
 * @since 20/03/2022
 */
public class ApplicationStarter {
    public static void main(String[] args) throws RemoteException, AlreadyBoundException {
        Registry registry = LocateRegistry.getRegistry(RmiUtils.RMI_REGISTRY_PORT);
        PrimeService primeService = new PrimeService();
        PrimeCalculationTask primeCalculationTask = new PrimeCalculationTask(registry, primeService);
        PrimeController primeController = new PrimeControllerImpl(primeCalculationTask);

        registry.bind("Prime", (PrimeController) UnicastRemoteObject.exportObject(primeController, 0));

        Executors.newSingleThreadExecutor().submit(primeCalculationTask);
    }
}
