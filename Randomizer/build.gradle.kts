plugins {
    `project-common`
    application
}

repositories {
    mavenLocal()
    mavenCentral()
}

application {
    mainClass.set("com.statestreet.randomizer.ApplicationStarter")
}

dependencies {
    implementation(project(":rmi-api"))
}
