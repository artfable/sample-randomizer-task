package com.statestreet.randomizer;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Random;
import java.util.concurrent.Executors;

import com.statestreet.randomizer.controller.RandomizerPrintControllerImpl;
import com.statestreet.randomizer.service.IntSenderService;
import com.statestreet.randomizer.service.RandomIntGeneratorService;
import com.statestreet.randomizer.service.RandomizerPrintService;
import com.statestreet.rmi.api.RmiUtils;
import com.statestreet.rmi.api.controller.RandomizerPrintController;

/**
 * @author artfable
 * @since 20/03/2022
 */
public class ApplicationStarter {
    public static void main(String[] args) throws RemoteException, AlreadyBoundException {
        Registry registry = LocateRegistry.getRegistry(RmiUtils.RMI_REGISTRY_PORT);
        RandomizerPrintService randomizerPrintService = new RandomizerPrintService(System.out);
        RandomizerPrintController randomizerPrintController = new RandomizerPrintControllerImpl(randomizerPrintService);

        registry.bind("RandomizerPrint", (RandomizerPrintController) UnicastRemoteObject.exportObject(randomizerPrintController, 0));

        Executors.newSingleThreadExecutor().submit(new IntSenderService(new RandomIntGeneratorService(new Random()), registry));
    }
}
