package com.statestreet.randomizer.controller;

import java.rmi.RemoteException;

import com.statestreet.randomizer.service.RandomizerPrintService;
import com.statestreet.rmi.api.controller.RandomizerPrintController;

/**
 * @author artfable
 * @since 20/03/2022
 */
public class RandomizerPrintControllerImpl implements RandomizerPrintController {

    private final RandomizerPrintService randomizerPrintService;

    public RandomizerPrintControllerImpl(RandomizerPrintService randomizerPrintService) {
        this.randomizerPrintService = randomizerPrintService;
    }

    @Override
    public void print(int number, boolean prime) throws RemoteException {
        randomizerPrintService.print(number, prime);
    }
}
