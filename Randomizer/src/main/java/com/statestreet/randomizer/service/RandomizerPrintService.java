package com.statestreet.randomizer.service;

import java.io.PrintStream;

/**
 * @author artfable
 * @since 20/03/2022
 */
public class RandomizerPrintService {

    private final PrintStream out;

    public RandomizerPrintService(PrintStream out) {
        this.out = out;
    }

    public void print(int number, boolean prime) {
        out.println(number + " is prime: " + prime);
    }
}
