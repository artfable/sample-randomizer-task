package com.statestreet.randomizer.service;

import java.util.random.RandomGenerator;

/**
 * @author artfable
 * @since 20/03/2022
 */
public class RandomIntGeneratorService {
    private final RandomGenerator random;

    public RandomIntGeneratorService(RandomGenerator random) {
        this.random = random;
    }

    public int getNextInt() {
        return random.nextInt(1, Integer.MAX_VALUE);
    }
}
