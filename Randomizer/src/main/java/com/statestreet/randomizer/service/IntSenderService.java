package com.statestreet.randomizer.service;

import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;

import com.statestreet.rmi.api.RmiUtils;
import com.statestreet.rmi.api.controller.PrimeController;

/**
 * @author artfable
 * @since 20/03/2022
 */
public class IntSenderService implements Runnable {
    private final RandomIntGeneratorService randomIntGeneratorService;
    private final Registry registry;

    public IntSenderService(RandomIntGeneratorService randomIntGeneratorService, Registry registry) {
        this.randomIntGeneratorService = randomIntGeneratorService;
        this.registry = registry;
    }

    @Override
    public void run() {
        String primeServiceName = "Prime";
        PrimeController primeController;

        try {
            RmiUtils.waitForRmiService(primeServiceName, registry);
            primeController = (PrimeController) registry.lookup(primeServiceName);
        } catch (RemoteException | NotBoundException e) {
            throw new RuntimeException(e);
        }
        while (true) {
            try {
                primeController.sendInt(randomIntGeneratorService.getNextInt());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }
}
